<%--
  Created by IntelliJ IDEA.
  User: 59013-36-05
  Date: 10/10/2019
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="Competences" scope="request" class="beans.Competences"></jsp:useBean>
<jsp:setProperty name="Competences" property="*"></jsp:setProperty>
<jsp:setProperty name="Competences" property="competencesSuccess" value='${pageContext.request.getParameterValues("touteslescompetences")}'/>
<html>
<head>
    <title>Bilan</title>
</head>
<body>
<H2>

</H2>

<ul> les competences acquises : <br>
    <c:forEach var="CompetencesAcquises" items="${Competences.competencesSuccess}">
        <li> ${CompetencesAcquises}</li>
    </c:forEach>
</ul>    <br>
<ul>
    les competences non acquises : <br>
    <c:forEach var="CompetencesNonAcquises" items="${Competences.competencesFailed}">
        <li> ${CompetencesNonAcquises}</li>
    </c:forEach>
</ul>


</body>
</html>
