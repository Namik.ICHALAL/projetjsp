<%--
  Created by IntelliJ IDEA.
  User: 59013-36-05
  Date: 10/10/2019
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="Competences" scope="request" class="beans.Competences"></jsp:useBean>
<jsp:setProperty name="Competences" property="competences"></jsp:setProperty>
<jsp:setProperty name="Competences" property="*"></jsp:setProperty>

<html>
<head>
    <title>Gite de France</title>
</head>

    <body>
    <div class="jumbotron">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="PhotosCouvertures/1.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="PhotosCouvertures\2.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="/PhotosCouvertures/3.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
    <h2>Evaluation des compétences</h2>
    <form method="post" action="bilan.jsp">
    <table>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <c:forEach var="competence" items="${Competences.competences}">
            <tr>
                <td>

                    <input type="checkbox" id="lescompetences" name="touteslescompetences" value=${competence}
                           >
                    <label for="lescompetences">${competence}</label>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td>
                <input type="submit" name="valider" value="Valider">
            </td>
        </tr>
    </table>
    </form>
    </body>

</html>
