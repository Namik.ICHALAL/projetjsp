package metier;

public class TypeEquipement {
	
	  private int idTypeServiceEquipement;
	    private String nomtTypeServiceEquipement;

	public TypeEquipement(int idTypeServiceEquipement, String nomtTypeServiceEquipement) {
		this.idTypeServiceEquipement = idTypeServiceEquipement;
		this.nomtTypeServiceEquipement = nomtTypeServiceEquipement;
	}

	public int getIdTypeServiceEquipement() {
		return idTypeServiceEquipement;
	}

	public TypeEquipement setIdTypeServiceEquipement(int idTypeServiceEquipement) {
		this.idTypeServiceEquipement = idTypeServiceEquipement;
		return this;
	}

	public String getNomtTypeServiceEquipement() {
		return nomtTypeServiceEquipement;
	}

	public TypeEquipement setNomtTypeServiceEquipement(String nomtTypeServiceEquipement) {
		this.nomtTypeServiceEquipement = nomtTypeServiceEquipement;
		return this;
	}

	@Override
	public String toString() {
		return nomtTypeServiceEquipement ;
	}
}
