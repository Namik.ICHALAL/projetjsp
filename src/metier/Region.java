package metier;

public class Region {
    private String codeRegion;
    private String nomRegion;

    public Region(String codeRegion, String nomRegion) {
        this.codeRegion = codeRegion;
        this.nomRegion = nomRegion;
    }

    public String getCodeRegion() {
        return codeRegion;
    }

    public Region setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
        return this;
    }

    public String getNomRegion() {
        return nomRegion;
    }

    public Region setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
        return this;
    }

    @Override
    public String toString() {
        return  nomRegion ;
    }
}
