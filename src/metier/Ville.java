package metier;



import java.util.ArrayList;

public class Ville {
  private String codeInsee;
  private String nomVille;
  private String codePostale;
  private float latitude;
  private float lomgitude;
  private Departement departement;

    public Ville(String codeInsee, String nomVille, String codePostale, float latitude, float lomgitude) {
        this.codeInsee = codeInsee;
        this.nomVille = nomVille;
        this.codePostale = codePostale;
        this.latitude = latitude;
        this.lomgitude = lomgitude;
        this.departement =departement;
    }

    public String getCodeInsee() {
        return codeInsee;
    }

    public Ville setCodeInsee(String codeInsee) {
        this.codeInsee = codeInsee;
        return this;
    }

    public String getNomVille() {
        return nomVille;
    }

    public Ville setNomVille(String nomVille) {
        this.nomVille = nomVille;
        return this;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public Ville setCodePostale(String codePostale) {
        this.codePostale = codePostale;
        return this;
    }

    public float getLatitude() {
        return latitude;
    }

    public Ville setLatitude(float latitude) {
        this.latitude = latitude;
        return this;
    }

    public float getLomgitude() {
        return lomgitude;
    }

    public Ville setLomgitude(float lomgitude) {
        this.lomgitude = lomgitude;
        return this;
    }


    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    @Override
    public String toString() {
        return  nomVille ;

    }
}
