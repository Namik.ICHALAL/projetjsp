package metier;

import java.math.BigDecimal;

public class Tarification {
    private BigDecimal tarif;
    private Periode periode;

    public Tarification(BigDecimal tarif) {
        this.tarif = tarif;
        this.periode = periode;
    }

    public BigDecimal getTarif() {
        return tarif;
    }

    public Tarification setTarif(BigDecimal tarif) {
        this.tarif = tarif;
        return this;
    }

    public Periode getPeriode() {
        return periode;
    }

    public Tarification setPeriode(Periode periode) {
        this.periode = periode;
        return this;
    }
}
