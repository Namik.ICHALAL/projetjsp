package metier;



public class Telephone {

    private int idTelephone;
    private String TypeTelephone;
    private String numeroTelephone;

	public Telephone(int idTelephone, String typeTelephone, String numeroTelephone) {
		this.idTelephone = idTelephone;
		TypeTelephone = typeTelephone;
		this.numeroTelephone = numeroTelephone;
	}

	public int getIdTelephone() {
		return idTelephone;
	}

	public Telephone setIdTelephone(int idTelephone) {
		this.idTelephone = idTelephone;
		return this;
	}

	public String getTypeTelephone() {
		return TypeTelephone;
	}

	public Telephone setTypeTelephone(String typeTelephone) {
		TypeTelephone = typeTelephone;
		return this;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public Telephone setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
		return this;
	}

	@Override
	public String toString() {
		return  numeroTelephone ;
	}
}
