package metier;

import java.math.BigDecimal;

public class Prestation {
    private BigDecimal prix;
    private Object equipementService;

    public BigDecimal getPrix() {
        return prix;
    }

    public Prestation setPrix(BigDecimal prix) {
        this.prix = prix;
        return this;
    }

    public Object getEquipementService() {
        return equipementService;
    }

    public Prestation setEquipementService(Object equipementService) {
        this.equipementService = equipementService;
        return this;
    }

    public Prestation(BigDecimal prix) {
        this.prix = prix;
        this.equipementService= equipementService;


    }
}
