package DAO;

import metier.TypePeriode;

import java.sql.Connection;
import java.util.ArrayList;

public class TypePeriodeDAO extends DAO<TypePeriode>{
    public TypePeriodeDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypePeriode getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypePeriode> getAll() {
        return null;
    }

    @Override
    public boolean insert(TypePeriode objet) {
        return false;
    }

    @Override
    public boolean update(TypePeriode objet) {
        return false;
    }

    @Override
    public boolean delete(TypePeriode objet) {
        return false;
    }
}
