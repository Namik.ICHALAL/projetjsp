package DAO;

import metier.Departement;
import metier.Region;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DepartementDAO extends DAO<Departement> {
    public DepartementDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Departement getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Departement> getAll() {
        ResultSet rs;
        ArrayList<Departement> liste = new ArrayList<Departement>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  code_departement,nom_departement,r.code_region, r.nom_region from departement as d \n" +
                    " JOIN region r on d.code_region = r.code_region\n" +
                    "order by nom_departement";

            rs = stmt.executeQuery(strCmd);


            Departement  departement ;
            while (rs.next())
            { departement = new Departement(rs.getString(1), rs.getString(2));
               departement.setRegion(new Region(rs.getString(3), rs.getString(4)));
               liste.add(departement);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("récuperation des données departement impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Departement objet) {
        return false;
    }

    @Override
    public boolean update(Departement objet) {
        return false;
    }

    @Override
    public boolean delete(Departement objet) {
        return false;
    }
}
