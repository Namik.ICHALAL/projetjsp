package DAO;

import metier.*;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.SimpleTimeZone;

public class GiteDAO extends DAO<Gite>{

    public GiteDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Gite getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Gite> getAll() {
        ResultSet rs;
        ArrayList<Gite> liste = new ArrayList<Gite>();
        try
        {
            Statement stmt = null;

                stmt = connexion.createStatement();

                String strCmd = "SELECT g.id_gite,nom_gite,\n" +
                        "                        surface_habitable,nombre_chambres, nombre_couchage,site_web,adresse_1,adresse_2,\n" +
                        "                                                   adresse_3,libelle_voie,g.code_postale,\n" +
                        "                        c2.code_insee,c2.nom_commune,c2.code_postale,c2.latitude,c2.longitude,\n" +
                        "                        p1.id_personne,p1.nom_personne,p1.prenom_personne,p1.email,\n" +
                        "                        p2.id_personne,p2.nom_personne,p2.prenom_personne,p2.email," +
                        "\n" +

                        "                        d.code_departement,d.nom_departement,\n" +
                        "                        r.code_region,r.nom_region\n" +
                        "                        from gite\n" +
                        "                        as G join commune c2 on G.code_departement = c2.code_departement and G.code_insee = c2.code_insee\n" +
                        "                            JOIN est_gere_par E on E.id_gite=g.id_gite\n" +
                        "                           join personne as p1 ON E.id_personne=p1.id_personne\n" +
                        "                            JOIN avoir A on A.id_gite=g.id_gite\n" +
                        "                           join personne as p2 ON E.id_personne=p2.id_personne\n" +

                        "                        join  departement d on c2.code_departement = d.code_departement\n" +
                        "                        JOIN region r on d.code_region = r.code_region\n" +
                        "                        order by id_gite";

                rs = stmt.executeQuery(strCmd);
                Personne proprietaire;
                Personne gerant;
                 Gite  gite ;
                 Ville ville;
                 Departement departement;
                while (rs.next())
                { gite = new Gite(rs.getInt(1), rs.getString(2));
                            gite.setNombreChambres(rs.getInt(4));
                            gite.setNombreCouchages(rs.getInt(5));
                            gite.setAdresse1(rs.getString(7));
                            gite.setAdresse2(rs.getString(8));
                            gite.setAdresse3(rs.getString(9));
                            gite.setLibelleVoie(rs.getString(10));
                            gite.setCodePostale(rs.getString(11));
                            gite.setSurfaceHabitable(rs.getFloat(3));
                            gite.setSiteWeb(rs.getString(6));
                            ville= new Ville(rs.getString(12), rs.getString(13),
                                    rs.getString(14),rs.getFloat(15),
                                    rs.getFloat(16));
                           gite.setVille(ville);
                           departement= new Departement(rs.getString(25),rs.getString(26));
                           ville.setDepartement(departement);
                           departement.setRegion(new Region(rs.getString(27),rs.getString(28)));
                           gite.setGerants(listerGerants(rs.getInt(1)));
                    gite.setProprietaires(listerProprietaire(rs.getInt(1)));
                    gite.setPrestations(listerPrestations(rs.getInt(1)));
                     setTarif(rs.getInt(1));


                       System.out.println(gite);
                        liste.add(gite);
                }
                rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

            System.out.println(liste.size());

        return liste;
    }

    private void setTarif(int idGite) {
        ResultSet rs;
        ArrayList<Tarification> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = " select t.tarif_hebdomadaire from   gite as G\n" +
                    "                                       JOIN tarifer as t on t.id_gite = g.id_gite\n" +
                    "                                       join periode p on p.id_periode= t.id_periode\n" +
                    "                                       join type_periode tp on p.id_type_periode = tp.id_type_periode\n" +
                    "where    (CAST (to_char(now(),'WW') AS INTEGER) = CAST (p.numero_semaine AS INTEGER)) and  G.id_gite = "+idGite;
            rs = stmt.executeQuery(strCmd);

            Tarification  tarification;
            while (rs.next()){



            }
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des Service_Equipement" + error);
        }

    }






    private ArrayList<Personne> listerProprietaire(int idGite ) {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  p.id_personne,p.nom_personne,p.prenom_personne,p.email from gite as G\n" +
                    "                    JOIN avoir A ON A.id_gite=g.id_gite \n " +
                    "                    join personne p on p.id_personne=a.id_personne \n" +

                    "                   where g.id_gite = "+ idGite;

            rs = stmt.executeQuery(strCmd);
            Personne  personne ;
            EquipementService equipementService;
            while (rs.next()){
                personne = new Personne(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
                personne.setTelephone(listerTelephone(rs.getInt(1)));
         //       personne.setDisponibilite(listerDisponibilite(rs.getInt(1)));
                liste.add(personne);
            }
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des Proprietaires " + error);
        }
        return liste;
    }
    private ArrayList<Personne> listerGerants(int idGite) {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  p.id_personne,p.nom_personne,p.prenom_personne,p.email from gite as G\n" +
                    "                    JOIN avoir A ON A.id_gite=g.id_gite \n " +
                    "                    join personne p on p.id_personne=a.id_personne \n" +

                    "                   where g.id_gite = "+ idGite;

            rs = stmt.executeQuery(strCmd);
            Personne  personne ;
            EquipementService equipementService;
            while (rs.next()){
                personne = new Personne(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
                personne.setTelephone(listerTelephone(rs.getInt(1)));
                personne.setDisponibilite(listerDisponibilite(rs.getInt(1)));
                liste.add(personne);
            }
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des  gerants " + error);
        }
        return liste;
    }

   private ArrayList<Disponibilite> listerDisponibilite(int idPersonne) {
  ResultSet rs;
    ArrayList<Disponibilite> liste = new ArrayList<>();


        try {
        Statement stmt = null;

        stmt = connexion.createStatement();

        String strCmd = "select  d.id_disponibilite,to_char(TO_DATE(d.jour,'DD'), 'Day ') , D.heure_debut, d.heure_fin from personne as p\n" +
                "    JOIN  disponibilite d on p.id_personne = d.id_personne\n" +
                "where p.id_personne = "+ idPersonne+ "order by d.jour";

        rs = stmt.executeQuery(strCmd);
        Disponibilite disponibilite ;


            Format formatter = new SimpleDateFormat("DD");

            Format format = new SimpleDateFormat("HH");
        while (rs.next()){
            disponibilite = new Disponibilite (rs.getInt(1),formatter.format(rs.getDate(2)),format.format(rs.getTime(3)),format.format(rs.getTime(4)));
            System.out.println(disponibilite);
            liste.add(disponibilite);
        }
    } catch (Exception error) {
        System.out.println("Impossible de récuperer la liste des  Disponibilité " + error);
    }
        return liste;

   }

    private ArrayList<Telephone> listerTelephone(int idPersonne) {
        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  tel.id_telephone,tel.type_telephone,tel.numero from personne as p\n" +
                    "                    JOIN possede ps ON ps.id_personne=p.id_personne \n " +
                    "                    join telephone TEL on TEL.id_telephone=PS.id_telephone \n" +
                    "                   where p.id_personne = "+ idPersonne;

            rs = stmt.executeQuery(strCmd);
            Telephone telephone ;
            EquipementService equipementService;
            while (rs.next()){
                telephone = new Telephone (rs.getInt(1),rs.getString(2),rs.getString(3) );
                liste.add(telephone);
            }
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des    Telephones " + error);
        }
        return liste;
    }


    private ArrayList<Prestation> listerPrestations(int idGite) {
        ResultSet rs;
        ArrayList<Prestation> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select p.prix,ees.id_service, ees.nom_service,ees.id_type_service, tes.libelle  from gite as G\n" +
                    "                    JOIN proposer P  on G.id_gite = P.id_gite\n" +
                    "                    join equipement_et_service ees on P.id_service = ees.id_service\n" +
                    "join type_equipement_service tes on ees.id_type_service = tes.id_type_service" +
                    " where g.id_gite = "+ idGite  ;
            rs = stmt.executeQuery(strCmd);
            Prestation  prestation ;
            EquipementService equipementService;
            while (rs.next()){
                prestation = new Prestation(rs.getBigDecimal(1));
                equipementService= new EquipementService(rs.getInt(2),rs.getString(3));
                prestation.setEquipementService(equipementService);
                equipementService.setTypeEquipementService(new TypeEquipement(rs.getInt(4),rs.getString(5)));
                liste.add(prestation);

            }
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des Service_Equipement" + error);
        }
return liste;
    }



    @Override
    public boolean insert(Gite objet) {
        return false;
    }

    @Override
    public boolean update(Gite objet) {
        return false;
    }

    @Override
    public boolean delete(Gite objet) {
        return false;
    }
}
