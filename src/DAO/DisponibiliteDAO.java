package DAO;

import metier.Disponibilite;

import java.sql.Connection;
import java.util.ArrayList;

public class DisponibiliteDAO extends DAO<Disponibilite> {
    public DisponibiliteDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Disponibilite getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Disponibilite> getAll() {
        return null;
    }

    @Override
    public boolean insert(Disponibilite objet) {
        return false;
    }

    @Override
    public boolean update(Disponibilite objet) {
        return false;
    }

    @Override
    public boolean delete(Disponibilite objet) {
        return false;
    }
}
