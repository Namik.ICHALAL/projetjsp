package DAO;

import metier.Region;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class RegionDAO extends DAO<Region>{
    public RegionDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Region getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Region> getAll() {
        ResultSet rs;
        ArrayList<Region> liste = new ArrayList<Region>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  code_region, nom_region from region order by nom_region";

            rs = stmt.executeQuery(strCmd);


           Region  region ;
            while (rs.next())
            { region = new Region(rs.getString(1), rs.getString(2));
            liste.add(region);}
            rs.close();
        } catch (Exception error) {
            System.out.println("récuperation des données Region impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Region objet) {
        return false;
    }

    @Override
    public boolean update(Region objet) {
        return false;
    }

    @Override
    public boolean delete(Region objet) {
        return false;
    }
}
