package DAO;

import metier.TypeEquipement;

import java.sql.Connection;
import java.util.ArrayList;

public class TypeEquipementServiceDAO extends DAO<TypeEquipement>{
    public TypeEquipementServiceDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypeEquipement getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypeEquipement> getAll() {
        return null;
    }

    @Override
    public boolean insert(TypeEquipement objet) {
        return false;
    }

    @Override
    public boolean update(TypeEquipement objet) {
        return false;
    }

    @Override
    public boolean delete(TypeEquipement objet) {
        return false;
    }
}
