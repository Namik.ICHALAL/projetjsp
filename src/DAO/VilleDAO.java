package DAO;

import metier.Departement;
import metier.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VilleDAO extends DAO<Ville> {

    public VilleDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Ville getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Ville> getAll() {
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<Ville>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  code_insee,nom_commune,code_postale,latitude,longitude,d.code_departement,d.nom_departement\n" +
                    "from commune as c2 JOIN departement d on c2.code_departement = d.code_departement\n" +
                    "order by C2.nom_commune";

            rs = stmt.executeQuery(strCmd);


            Ville ville   ;
            while (rs.next())
            { ville = new Ville(rs.getString(1), rs.getString(2),
                    rs.getString(3),rs.getFloat(4),
                    rs.getFloat(5));
                ville.setDepartement(new Departement(rs.getString(6), rs.getString(7)));
             liste.add(ville);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Ville objet) {
        return false;
    }

    @Override
    public boolean update(Ville objet) {
        return false;
    }

    @Override
    public boolean delete(Ville objet) {
        return false;
    }
}
