package beans;

import java.util.ArrayList;
import java.util.Arrays;

public class Competences
{
    private String nom;
    private ArrayList<String> competences;
    private String[] competencesSuccess;
    private ArrayList<String> competencesFailed;

    public Competences()
    {
        this.competences = new ArrayList<>(Arrays.asList("Class", "Controller", "JavaFX", "Héritage", "JEE"));
        this.competencesFailed = new ArrayList<>();
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public ArrayList getCompetences()
    {
        return competences;
    }

    public void setCompetences(ArrayList competences)
    {
        this.competences = competences;
    }

    public String[] getCompetencesSuccess()
    {
        return competencesSuccess;
    }

    public void setCompetencesSuccess(String[] competencesSuccess)
    {
        this.competencesSuccess = competencesSuccess;
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(competencesSuccess));

        for (String skill : competences)
        {
            if (!list.contains(skill))
            {
                competencesFailed.add(skill);
            }
        }
    }

    public ArrayList getCompetencesFailed()
    {
        return competencesFailed;
    }

    public void setCompetencesFailed(ArrayList competencesFailed)
    {
        this.competencesFailed = competencesFailed;
    }

}
